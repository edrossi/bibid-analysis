/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "BIBIDAnalysis.h"
#include "StoreGate/ReadHandle.h"

#include "TTree.h"
#include "TString.h"

#include <algorithm>
#include <math.h>
#include <functional>
#include <iostream>

BIBIDAnalysis::BIBIDAnalysis(const std::string& name, ISvcLocator* pSvcLocator)
  : AthAlgorithm(name, pSvcLocator)
  , m_EventNumber(0)
  , m_RunNumber(0)
  , m_TimeStamp(0)
  , m_LBNumber(0)
  , m_BCID(0)
  , m_GRL(false)
  , m_ACTrigger(false)
  , m_CATrigger(false)
  , m_isoTrigger(false)
  , m_BCMprescale(-1)
  , m_HLTSCTTrigger(false)
  , m_HLTSCTprescale(-1)
  , m_isBeam1(false)
  , m_isBeam2(false)
  , m_isUnpaired(false)
  , m_gapOppositeBefore(0)
  , m_gapOppositeAfter(0)
  , m_gapSameBefore(0)
  , m_gapSameAfter(0)
  , m_intensityBeam(0)
  , m_LBDuration(0)
  , m_VertexTracks(0)
  , m_VertexType(0)
  , m_VertexChi2(0)
  , m_VertexDoF(0)
  , m_VertexX(0)
  , m_VertexY(0)
  , m_VertexZ(0)
  , m_VertexpT2(0)
  , m_BCMchan(0)
  , m_BCMpulse1Pos(0)
  , m_BCMpulse1Width(0)
  , m_BCMpulse2Pos(0)
  , m_BCMpulse2Width(0)
  , m_BCMBCID(0)
  , m_PixelHitsOnDisks(0)
  , m_PixelRDOOnDisks(0)
  , m_SCTHitsOnDisks(0)
  , m_SCTRDOOnDisks(0)
  , m_TRTHitsOnDisks(0)
  , m_confBCID(0)
  , m_bcIntensities(0)

  , m_tree(0)
  , m_ntupleFileName("/output")
  , m_ntupleTreeName("BIBIDAna")
  , m_path("/BIBIDAnalysis/")
  , m_thistSvc("THistSvc", name)
  , m_trigDecTool("Trig::TrigDecisionTool/TrigDecisionTool")
  , m_GRLTool("GoodRunsListSelectionTool/GRLTool")
  , m_bunchCrossingTool("Trig::LHCBunchCrossingTool/BunchCrossingTool")
  , m_bcConf( "Trig::LHCBunchCrossingTool/BunchCrossingTool" )
  , m_lumiTool("LuminosityTool")
{
  declareProperty("NtupleFileName", m_ntupleFileName);
  declareProperty("NtupleTreeName", m_ntupleTreeName);
  declareProperty("HistPath", m_path);
  declareProperty("BCTool", m_bunchCrossingTool);
  declareProperty("BCConfTool", m_bcConf);
  declareProperty("LumiTool",m_lumiTool);
}

StatusCode BIBIDAnalysis::initialize() {
  ATH_MSG_DEBUG( "Initializing BIBIDAnalysis" );

  // Grab Tools
  ATH_CHECK(m_thistSvc.retrieve());
  m_GRLTool.setTypeAndName( "GoodRunsListSelectionTool/GRLTool" ); 
  ATH_CHECK( m_GRLTool.retrieve() );
  ATH_CHECK(m_trigDecTool.retrieve());
  ATH_CHECK(m_bunchCrossingTool.retrieve());
  ATH_CHECK(m_bcConf.retrieve());
  ATH_CHECK(m_lumiTool.retrieve());

  m_tree = new TTree(TString(m_ntupleTreeName), "BIBIDAna");
  std::string fullNtupleName = m_ntupleFileName + m_ntupleTreeName;
  ATH_CHECK(m_thistSvc->regTree(fullNtupleName, m_tree));
  if (m_tree) {
    // TREE BRANCHES
    m_tree->Branch("EventNumber", &m_EventNumber);
    m_tree->Branch("RunNumber", &m_RunNumber);
    m_tree->Branch("TimeStamp", &m_TimeStamp);
    m_tree->Branch("LBNumber", &m_LBNumber);
    m_tree->Branch("BCID", &m_BCID);
    m_tree->Branch("GoodLB", &m_GRL);
    m_tree->Branch("ACTrigger", &m_ACTrigger);
    m_tree->Branch("CATrigger", &m_CATrigger);
    m_tree->Branch("isoTrigger", &m_isoTrigger);
    m_tree->Branch("BCMprescale", &m_BCMprescale);
    m_tree->Branch("HLTSCTTrigger", &m_HLTSCTTrigger);
    m_tree->Branch("HLTSCTprescale", &m_HLTSCTprescale); 
    m_tree->Branch("isBeam1", &m_isBeam1); 
    m_tree->Branch("isBeam2", &m_isBeam2); 
    m_tree->Branch("isUnpaired", &m_isUnpaired); 
    m_tree->Branch("gapOppositeBefore", &m_gapOppositeBefore); 
    m_tree->Branch("gapOppositeAfter", &m_gapOppositeAfter); 
    m_tree->Branch("gapSameBefore", &m_gapSameBefore); 
    m_tree->Branch("gapSameAfter", &m_gapSameAfter); 
    m_tree->Branch("IntensityBeam", &m_intensityBeam); 
    m_tree->Branch("LBDuration", &m_LBDuration); 
    m_tree->Branch("VertexTracks", &m_VertexTracks); 
    m_tree->Branch("VertexType", &m_VertexType); 
    m_tree->Branch("VertexChi2", &m_VertexChi2); 
    m_tree->Branch("VertexDoF", &m_VertexDoF); 
    m_tree->Branch("VertexX", &m_VertexX); 
    m_tree->Branch("VertexY", &m_VertexY); 
    m_tree->Branch("VertexZ", &m_VertexZ); 
    m_tree->Branch("VertexpT2", &m_VertexpT2); 
    m_tree->Branch("BCMchan", &m_BCMchan);
    m_tree->Branch("BCMpulse1Pos", &m_BCMpulse1Pos);
    m_tree->Branch("BCMpulse1Width", &m_BCMpulse1Width);
    m_tree->Branch("BCMpulse2Pos", &m_BCMpulse2Pos);
    m_tree->Branch("BCMpulse2Width", &m_BCMpulse2Width);
    m_tree->Branch("BCMBCID", &m_BCMBCID);
    m_tree->Branch("PixelHitsOnDisks", &m_PixelHitsOnDisks);
    m_tree->Branch("PixelRDOOnDisks", &m_PixelRDOOnDisks);
    m_tree->Branch("SCTHitsOnDisks", &m_SCTHitsOnDisks);
    m_tree->Branch("SCTRDOOnDisks", &m_SCTRDOOnDisks);
    m_tree->Branch("TRTHitsOnDisks", &m_TRTHitsOnDisks);  
  }
  else {
    ATH_MSG_ERROR("No tree found!");
  }

  return StatusCode::SUCCESS;
}

StatusCode BIBIDAnalysis::execute() {
  ATH_MSG_DEBUG( "In BIBIDAnalysis::execute()" );

  // clear branches
  m_BCMchan->clear();
  m_BCMpulse1Pos->clear();
  m_BCMpulse1Width->clear();
  m_BCMpulse2Pos->clear();
  m_BCMpulse2Width->clear();
  m_BCMBCID->clear();
  m_PixelHitsOnDisks->clear();
  m_PixelRDOOnDisks->clear();
  m_SCTHitsOnDisks->clear();
  m_SCTRDOOnDisks->clear();
  m_TRTHitsOnDisks->clear();
  m_VertexTracks->clear();
  m_VertexType->clear();
  m_VertexChi2->clear();
  m_VertexDoF->clear();
  m_VertexX->clear();
  m_VertexY->clear();
  m_VertexZ->clear();
  m_VertexpT2->clear();
  m_confBCID.clear();
  m_bcIntensities.clear();
  
  // EventInfo begins
  SG::ReadHandle<xAOD::EventInfo> eventInfo ("EventInfo");
  if(eventInfo.isValid()){
    m_EventNumber = eventInfo->eventNumber();
    m_RunNumber = eventInfo->runNumber();
    m_TimeStamp = eventInfo->timeStamp();
    m_LBNumber = eventInfo->lumiBlock();
    m_BCID = eventInfo->bcid();
  }
  // EventInfo ends
  
  // GoodRunList begins
  m_GRL = m_GRLTool->passRunLB(*eventInfo);
  // GoodRunList ends
  
  // Trigger begins (maybe it can be done better with just isPassed("TriggerName"))
  m_ACTrigger = false;
  m_CATrigger = false;
  m_isoTrigger = false;
  m_BCMprescale = -1;
  
  auto chainGroupACiso =  m_trigDecTool->getChainGroup("L1_BCM_AC_UNPAIRED_ISO.*");
  auto chainGroupCAiso = m_trigDecTool->getChainGroup("L1_BCM_CA_UNPAIRED_ISO.*");

  auto chainGroupACnoniso =  m_trigDecTool->getChainGroup("L1_BCM_AC_UNPAIRED_NONISO.*");
  auto chainGroupCAnoniso = m_trigDecTool->getChainGroup("L1_BCM_CA_UNPAIRED_NONISO.*");	
  
  for(auto &trig : chainGroupACiso->getListOfTriggers()) {
    auto cgAC = m_trigDecTool->getChainGroup(trig);
    std::string thisTrigAC = trig;
    if( cgAC->isPassed() ) {
      m_ACTrigger = true;
      m_isoTrigger = true;
      m_BCMprescale = cgAC->getPrescale();
    }
  }
  for(auto &trig : chainGroupCAiso->getListOfTriggers()) {
    auto cgCA = m_trigDecTool->getChainGroup(trig);
    std::string thisTrigCA = trig;
    if( cgCA->isPassed() ) {
      m_CATrigger = true;
      m_isoTrigger = true;
      m_BCMprescale = cgCA->getPrescale();
    }
  }
  for(auto &trignoniso : chainGroupACnoniso->getListOfTriggers()) {
    auto cgACnoniso = m_trigDecTool->getChainGroup(trignoniso);
    std::string thisTrigACnoniso = trignoniso;
    if( cgACnoniso->isPassed() ) {
      m_ACTrigger = true;
      m_isoTrigger = false;
      m_BCMprescale = cgACnoniso->getPrescale();
    }
  }
  for(auto &trignoniso : chainGroupCAnoniso->getListOfTriggers()) {
  auto cgCAnoniso = m_trigDecTool->getChainGroup(trignoniso);
  std::string thisTrigCAnoniso = trignoniso;
  if( cgCAnoniso->isPassed() ) {
    m_CATrigger = true;
    m_isoTrigger = false;
    m_BCMprescale = cgCAnoniso->getPrescale();
    }
  }
  
  auto HLT_sp = m_trigDecTool->getChainGroup("HLT_mb_sp_ncb_L1RD0_UNPAIRED_ISO.*");

  m_HLTSCTTrigger = false;
  m_HLTSCTprescale = -1;
  
  for(auto &trig : HLT_sp->getListOfTriggers()) {
  auto cgHLT = m_trigDecTool->getChainGroup(trig);
  if(cgHLT->isPassed()) {
    m_HLTSCTTrigger = true;
    m_HLTSCTprescale = cgHLT->getPrescale();
    }
  }
  // Trigger ends
  
  // Fill Parameters begins 
  m_isBeam1 = m_bunchCrossingTool->isBeam1(m_BCID);
  m_isBeam2 = m_bunchCrossingTool->isBeam2(m_BCID);
  m_isUnpaired = m_bunchCrossingTool->isUnpaired(m_BCID);
  Trig::IBunchCrossingTool::BunchDistanceType Dtype = Trig::IBunchCrossingTool::BunchDistanceType::BunchCrossings;
  Trig::IBunchCrossingTool::BunchFillType typeUnpaired1 = Trig::IBunchCrossingTool::BunchFillType::UnpairedBeam1;
  Trig::IBunchCrossingTool::BunchFillType typeUnpaired2 = Trig::IBunchCrossingTool::BunchFillType::UnpairedBeam2;
  Trig::IBunchCrossingTool::BunchFillType typeColliding = Trig::IBunchCrossingTool::BunchFillType::CollidingBunch;
  int gapbeforeunpaired1 = m_bunchCrossingTool->gapBeforeBunch(m_BCID, Dtype, typeUnpaired1);
  int gapbeforeunpaired2 = m_bunchCrossingTool->gapBeforeBunch(m_BCID, Dtype, typeUnpaired2);
  int gapbeforecoll = m_bunchCrossingTool->gapBeforeBunch(m_BCID, Dtype, typeColliding);
  int gapbefore1 = std::min(gapbeforeunpaired1, gapbeforecoll);
  int gapbefore2 = std::min(gapbeforeunpaired2, gapbeforecoll);
  int gapafterunpaired1 = m_bunchCrossingTool->gapAfterBunch(m_BCID, Dtype, typeUnpaired1);
  int gapafterunpaired2 = m_bunchCrossingTool->gapAfterBunch(m_BCID, Dtype, typeUnpaired2);
  int gapaftercoll = m_bunchCrossingTool->gapAfterBunch(m_BCID, Dtype, typeColliding);
  int gapafter1 = std::min(gapafterunpaired1, gapaftercoll);
  int gapafter2 = std::min(gapafterunpaired2, gapaftercoll);
  if(m_isBeam1 && !(m_isBeam2)){
    m_gapOppositeBefore = gapbefore2;
    m_gapOppositeAfter = gapafter2;
    m_gapSameBefore = gapbefore1;
    m_gapSameAfter = gapafter1;
  }
  else if(m_isBeam2 && !(m_isBeam1)){
    m_gapOppositeBefore = gapbefore1;
    m_gapOppositeAfter = gapafter1;
    m_gapSameBefore = gapbefore2;
    m_gapSameAfter = gapafter2;
  }
  else { //just dumping the values for beam 1, better than nothing
    m_gapOppositeBefore = gapbefore2;
    m_gapOppositeAfter = gapafter2;
    m_gapSameBefore = gapbefore1;
    m_gapSameAfter = gapafter1;
  }

  m_intensityBeam = 0;
  
  if(m_isUnpaired){
    if (m_isBeam1){
      m_confBCID = m_bcConf->configuredUnpairedBCIDsBeam1();
      m_bcIntensities = m_bcConf->configuredUnpairedIntensitiesBeam1(); 
    } else if (m_isBeam2){
      m_confBCID = m_bcConf->configuredUnpairedBCIDsBeam2();
      m_bcIntensities = m_bcConf->configuredUnpairedIntensitiesBeam2(); 
    }    
    for (unsigned int ii=0; ii < m_bcIntensities.size(); ii++){
      if(m_confBCID.at(ii) == m_BCID) m_intensityBeam = m_bcIntensities.at(ii);
    }   
  }
  
  m_LBDuration = m_lumiTool->lbDuration();
  //Fill Parameters ends
  
  // Primary Vertices begins  
  SG::ReadHandle<xAOD::VertexContainer> primaryVertices ("PrimaryVertices");
  if(primaryVertices.isValid()){
    for (xAOD::VertexContainer::const_iterator pv_itr = primaryVertices->begin();  pv_itr != primaryVertices->end(); pv_itr++) {
      xAOD::Vertex* myVtx = (*pv_itr);
      if (myVtx->nTrackParticles() != 0) {
        m_VertexTracks->push_back(myVtx->nTrackParticles());
        m_VertexType->push_back(myVtx->vertexType());
        m_VertexChi2->push_back(myVtx->chiSquared());
        m_VertexDoF->push_back(myVtx->numberDoF());
        m_VertexX->push_back(myVtx->x());
        m_VertexY->push_back(myVtx->y());
        m_VertexZ->push_back(myVtx->z());
        m_VertexpT2->push_back(myVtx->auxdataConst<float>("sumPt2"));
      }
    }
  }
  // Primary Vertices ends
  
  // BCM begins
  SG::ReadHandle<BCM_RDO_Container> p_BCM_RDO_cont ("BCM_RDOs");
  if(p_BCM_RDO_cont.isValid()) {
    // loop over RDO container
    BCM_RDO_Container::const_iterator rdoCont_itr(p_BCM_RDO_cont->begin());
    const BCM_RDO_Container::const_iterator rdoCont_end(p_BCM_RDO_cont->end());
    for ( ; rdoCont_itr != rdoCont_end; ++rdoCont_itr ) {
      const DataVector<BCM_RawData>* p_BCM_RDO_coll(*rdoCont_itr);
      DataVector<BCM_RawData>::const_iterator rdo_itr(p_BCM_RDO_coll->begin());
      const DataVector<BCM_RawData>::const_iterator rdo_end(p_BCM_RDO_coll->end());
      for ( ; rdo_itr != rdo_end; ++rdo_itr ) {
        const int chan((*rdo_itr)->getChannel());
        const int pulse1Pos((*rdo_itr)->getPulse1Position());
        const int pulse1Width((*rdo_itr)->getPulse1Width());
        const int pulse2Pos((*rdo_itr)->getPulse2Position());
        const int pulse2Width((*rdo_itr)->getPulse2Width());
        const int BCIDB((*rdo_itr)->getBCID());
        if(pulse1Pos != 0){ //store only channels with a hit
          m_BCMchan->push_back(chan);
          m_BCMpulse1Pos->push_back(pulse1Pos);
          m_BCMpulse1Width->push_back(pulse1Width);
          m_BCMpulse2Pos->push_back(pulse2Pos);
          m_BCMpulse2Width->push_back(pulse2Width);
          m_BCMBCID->push_back(BCIDB);
        }
      }
    }
  }
  // BCM ends
  
  // Pixel begins
  int hitsPixel_ECA[3] = {0};
  int hitsPixel_ECC[3] = {0};
  int RDOsPixel_ECA[3] = {0};
  int RDOsPixel_ECC[3] = {0};
  
  SG::ReadHandle<xAOD::TrackMeasurementValidationContainer> p_pixelClus_cont ("PixelClusters");
  if(p_pixelClus_cont.isValid()) {
    // loop over cluster container
    xAOD::TrackMeasurementValidationContainer::const_iterator pclusCont_itr(p_pixelClus_cont->begin());
    const xAOD::TrackMeasurementValidationContainer::const_iterator pclusCont_end(p_pixelClus_cont->end());
    for ( ; pclusCont_itr != pclusCont_end; ++pclusCont_itr ) {
      int bec = (*pclusCont_itr)->auxdataConst<int>("bec");
      int layer = (*pclusCont_itr)->auxdataConst<int>("layer");
      if ((bec == -2) && (layer < 3)) hitsPixel_ECC[layer]++; //not sure the layer condition is necessary
      if ((bec == 2) && (layer < 3)) hitsPixel_ECA[layer]++;
      std::vector< uint64_t > prdoList = (*pclusCont_itr)->auxdataConst<std::vector< uint64_t >>("rdoIdentifierList");
      if ((bec == -2) && (layer < 3)) RDOsPixel_ECC[layer] = RDOsPixel_ECC[layer] + prdoList.size(); //not sure the layer condition is necessary
      if ((bec == 2) && (layer < 3)) RDOsPixel_ECA[layer] = RDOsPixel_ECA[layer] + prdoList.size();
    }
  }
  
  for (int ii = 0; ii < 3; ii++) m_PixelHitsOnDisks->push_back(hitsPixel_ECC[ii]);
  for (int ii = 0; ii < 3; ii++) m_PixelHitsOnDisks->push_back(hitsPixel_ECA[ii]);
  for (int ii = 0; ii < 3; ii++) m_PixelRDOOnDisks->push_back(RDOsPixel_ECC[ii]);
  for (int ii = 0; ii < 3; ii++) m_PixelRDOOnDisks->push_back(RDOsPixel_ECA[ii]);
  // Pixel ends
  
  // SCT begins
  int hitsSCT_ECA[9] = {0};
  int hitsSCT_ECC[9] = {0};
  int RDOsSCT_ECA[9] = {0};
  int RDOsSCT_ECC[9] = {0};
  
  // get containers -- fill branches + histos
  SG::ReadHandle<xAOD::TrackMeasurementValidationContainer> p_sctClus_cont ("SCT_Clusters");
  if(p_sctClus_cont.isValid()) {
    // loop over cluster container
    xAOD::TrackMeasurementValidationContainer::const_iterator clusCont_itr(p_sctClus_cont->begin());
    const xAOD::TrackMeasurementValidationContainer::const_iterator clusCont_end(p_sctClus_cont->end());
    for ( ; clusCont_itr != clusCont_end; ++clusCont_itr ) {
      int bec = (*clusCont_itr)->auxdataConst<int>("bec");
      int layer = (*clusCont_itr)->auxdataConst<int>("layer");
      //int side = (*clusCont_itr)->auxdataConst<int>("side"); // each layer has two sides (0 and 1)
      if ((bec == -2) && (layer < 9)) hitsSCT_ECC[layer]++; //not sure the layer condition is necessary
      if ((bec == 2) && (layer < 9)) hitsSCT_ECA[layer]++;
      std::vector< uint64_t > rdoList = (*clusCont_itr)->auxdataConst<std::vector< uint64_t >>("rdoIdentifierList");
      if ((bec == -2) && (layer < 9)) RDOsSCT_ECC[layer] = RDOsSCT_ECC[layer] + rdoList.size(); //not sure the layer condition is necessary
      if ((bec == 2) && (layer < 9)) RDOsSCT_ECA[layer] = RDOsSCT_ECA[layer] + rdoList.size();
    }
  }
  
  for (int ii = 0; ii < 9; ii++) m_SCTHitsOnDisks->push_back(hitsSCT_ECC[ii]);
  for (int ii = 0; ii < 9; ii++) m_SCTHitsOnDisks->push_back(hitsSCT_ECA[ii]);
  for (int ii = 0; ii < 9; ii++) m_SCTRDOOnDisks->push_back(RDOsSCT_ECC[ii]);
  for (int ii = 0; ii < 9; ii++) m_SCTRDOOnDisks->push_back(RDOsSCT_ECA[ii]);
  // SCT ends
  
  // TRT begins
  int hitsTRT_ECA[14] = {0};
  int hitsTRT_ECC[14] = {0};
  
  SG::ReadHandle<xAOD::TrackMeasurementValidationContainer> p_trtClus_cont ("TRT_DriftCircles");
  if(p_trtClus_cont.isValid()) {
    // loop over cluster container
    xAOD::TrackMeasurementValidationContainer::const_iterator trtclusCont_itr(p_trtClus_cont->begin());
    const xAOD::TrackMeasurementValidationContainer::const_iterator trtclusCont_end(p_trtClus_cont->end());
    for ( ; trtclusCont_itr != trtclusCont_end; ++trtclusCont_itr ) {
      int bec = (*trtclusCont_itr)->auxdataConst<int>("bec");
      int layer = (*trtclusCont_itr)->auxdataConst<int>("layer");
      if (bec == -2) hitsTRT_ECC[layer]++;
      if (bec == 2) hitsTRT_ECA[layer]++;
    }
  }
  
  for (int ii = 0; ii < 14; ii++) m_TRTHitsOnDisks->push_back(hitsTRT_ECC[ii]++);
  for (int ii = 0; ii < 14; ii++) m_TRTHitsOnDisks->push_back(hitsTRT_ECA[ii]++);
  //TRT ends
  
  if (m_tree && !(m_ACTrigger == false && m_CATrigger == false && m_HLTSCTTrigger == false)) {
    m_tree->Fill();
  }

  return StatusCode::SUCCESS;
}

StatusCode BIBIDAnalysis::finalize() {
  return StatusCode::SUCCESS;
}
