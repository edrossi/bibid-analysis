#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../BIBIDAnalysis.h"

DECLARE_ALGORITHM_FACTORY( BIBIDAnalysis )

DECLARE_FACTORY_ENTRIES( BIBIDAnalysis ) {
  DECLARE_ALGORITHM( BIBIDAnalysis )
}
