#!/usr/bin/env bash

# Set-up the environment
setupATLAS

voms-proxy-init -voms atlas

lsetup rucio

# Select the latest list file
filename=$(ls List*.txt | tail -1)

# Create and enter in the folder for the output files
filename2=$(echo "$filename" | cut -f 1 -d '.')
mkdir -p ${filename2//List/OutputFiles}
cd ${filename2//List/OutputFiles}

# Download the files listed in the txt
while read -r line; do
    rucio download $line
done < "../$filename"

# Merge all the files using root (hadd)
filename3=$(echo "$filename2" | cut -c 5-)
outputname=final${filename3}.root
hadd -f $outputname user.*/user.*.root

# Delete variables
unset filename
unset filename2
unset filename3
unset outputname
