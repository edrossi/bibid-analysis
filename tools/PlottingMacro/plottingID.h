//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Mar 23 13:46:05 2017 by ROOT version 6.04/16
// from TTree tree/tree
// found on file: sample.root
//////////////////////////////////////////////////////////

#ifndef plottingID_h
#define plottingID_h

#include <TROOT.h>
#include <TChain.h>
#include <TProfile.h>
#include <TFile.h>
#include <TSelector.h>

#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "TH1I.h"
#include "TH2F.h"
#include "TGraph.h"

#include "TSystem.h"
#include "TEnv.h"
#include <fstream>
class plottingID : public TSelector {
private:
   std::string outputfile_;

public :
   TFile *outputFile = 0;

   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
 
// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t         EventNumber;
   Int_t         RunNumber;
   Int_t         timeStamp;
   Int_t         LBNumber;
   Int_t         bcid;
   Bool_t        passesACTrigger;
   Bool_t        passesCATrigger;
   Bool_t        isotrigger;
   Bool_t        GRL;
   vector<int>   *HitsOnDisks;
   Float_t       BCMprescale;
   Bool_t        passesHLT;
   Float_t       HLTprescale;
   vector<int>   *VertexTracks;
   vector<int>   *VertexType;
   vector<float> *VertexChi2;
   vector<float> *VertexDoF;
   vector<float> *VertexX;
   vector<float> *VertexY;
   vector<float> *VertexZ;
   vector<float> *VertexpT2;
   Int_t         HitsTRTECA;
   Int_t         HitsTRTECC;
   vector<int>   *HitsPixel;
   vector<int>   *BCMchan;
   vector<int>   *BCMBCID;
   vector<int>   *BCMpulse1Pos;
   vector<int>   *BCMpulse1Width;
   vector<int>   *BCMpulse2Pos;
   vector<int>   *BCMpulse2Width;
   
   Int_t 	  fNumberOfEvents; // Total number of events

   TTreeReader fReader;

   // List of branches
   TBranch *b_EventNumber;  
   TBranch *b_RunNumber;  
   TBranch *b_timeStamp; 
   TBranch *b_LBNumber;  
   TBranch *b_bcid;   
   TBranch *b_passesACTrigger;
   TBranch *b_passesCATrigger;   
   TBranch *b_HitsOnDisks;   
   TBranch *b_BCMprescale; 
   TBranch *b_passesHLT; 
   TBranch *b_HLTprescale; 
   TBranch *b_GRL;
   TBranch *b_isotrigger;
   TBranch *b_VertexTracks;
   TBranch *b_VertexType;
   TBranch *b_VertexChi2;
   TBranch *b_VertexDoF;
   TBranch *b_VertexX;
   TBranch *b_VertexY;
   TBranch *b_VertexZ;
   TBranch *b_VertexpT2;
   TBranch *b_HitsTRTECA;
   TBranch *b_HitsTRTECC;
   TBranch *b_HitsPixel;
   TBranch *b_BCMchan;
   TBranch *b_BCMBCID;
   TBranch *b_BCMpulse1Pos;
   TBranch *b_BCMpulse1Width;
   TBranch *b_BCMpulse2Pos;
   TBranch *b_BCMpulse2Width;

   // Readers to access the data: This is to read the variable in the private ntuple in MySelector.C file.
   TTreeReaderValue<Int_t> fEventNumber = {fReader, "EventNumber"};
   TTreeReaderValue<Int_t> fRunNumber = {fReader, "RunNumber"};
   TTreeReaderValue<Int_t> ftimeStamp = {fReader, "TimeStamp"};
   TTreeReaderValue<Int_t> fLBNumber = {fReader, "LBNumber"};
   TTreeReaderValue<Int_t> fbcid = {fReader, "BCID"};
   TTreeReaderValue<Bool_t> fpassesACTrigger = {fReader, "ACTrigger"};
   TTreeReaderValue<Bool_t> fpassesCATrigger = {fReader, "CATrigger"};
   TTreeReaderValue<std::vector<int>> fHitsOnDisks = {fReader, "SCTHitsOnDisks"};
   TTreeReaderValue<Float_t> fBCMprescale = {fReader, "BCMprescale"};
   TTreeReaderValue<Bool_t> fpassesHLT = {fReader, "HLTSCTTrigger"};
   TTreeReaderValue<Float_t> fHLTprescale = {fReader, "HLTSCTprescale"};
   TTreeReaderValue<Bool_t> fisotrigger = {fReader, "isoTrigger"};
   TTreeReaderValue<Bool_t> fGRL = {fReader, "GoodLB"};
   TTreeReaderValue<std::vector<int>> fVertexTracks = {fReader, "VertexTracks"};
   TTreeReaderValue<std::vector<int>> fVertexType = {fReader, "VertexType"};
   TTreeReaderValue<std::vector<float>> fVertexChi2 = {fReader, "VertexChi2"};
   TTreeReaderValue<std::vector<float>> fVertexDoF = {fReader, "VertexDoF"};
   TTreeReaderValue<std::vector<float>> fVertexX = {fReader, "VertexX"};
   TTreeReaderValue<std::vector<float>> fVertexY = {fReader, "VertexY"};
   TTreeReaderValue<std::vector<float>> fVertexZ = {fReader, "VertexZ"};
   TTreeReaderValue<std::vector<float>> fVertexpT2 = {fReader, "VertexpT2"};
   TTreeReaderValue<Int_t> fHitsTRTECA = {fReader, "TRTHitsECA"};
   TTreeReaderValue<Int_t> fHitsTRTECC = {fReader, "TRTHitsECC"};
   TTreeReaderValue<std::vector<int>> fHitsPixel = {fReader, "PixelHitsOnDisks"};
   TTreeReaderValue<std::vector<int>> fBCMchan = {fReader, "BCMchan"};
   TTreeReaderValue<std::vector<int>> fBCMBCID = {fReader, "BCMBCID"};
   TTreeReaderValue<std::vector<int>> fBCMpulse1Pos = {fReader, "BCMpulse1Pos"};
   TTreeReaderValue<std::vector<int>> fBCMpulse1Width = {fReader, "BCMpulse1Width"};
   TTreeReaderValue<std::vector<int>> fBCMpulse2Pos = {fReader, "BCMpulse2Pos"};
   TTreeReaderValue<std::vector<int>> fBCMpulse2Width = {fReader, "BCMpulse2Width"};
   
   plottingID(TTree * /*tree*/ =0) : 
 	fNumberOfEvents(0),
	fChain(0) { }

   virtual ~plottingID() { }

   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   TProfile * h_ACoccupancydisktimestamp[9];
   TProfile * h_CAoccupancydisktimestamp[9];
   TProfile * h_ACoccupancydisknonisotimestamp[9];
   TProfile * h_CAoccupancydisknonisotimestamp[9];
   TProfile * h_ACasymmetrytimestamp[9];
   TProfile * h_CAasymmetrytimestamp[9];
   TProfile * h_ACoccupancyTRTtimestamp;
   TProfile * h_CAoccupancyTRTtimestamp;
   TProfile * h_CAasymmetryTRTtimestamp;
   TProfile * h_ACasymmetryTRTtimestamp;
   TProfile * h_ACoccupancyPixeltimestamp;
   TProfile * h_CAoccupancyPixeltimestamp;
   TProfile * h_CAasymmetryPixeltimestamp;
   TProfile * h_ACasymmetryPixeltimestamp;
   TH1I * h_asymCA[9];
   TH1I * h_asymAC[9];
   TH2F * h_SCTTRT2DAC;
   TH2F * h_SCTPixel2DAC;
   TH2F * h_SCTTRT2DCA;
   TH2F * h_SCTPixel2DCA;
   TH2F * h_PixelTRT2DAC;
   TH2F * h_PixelTRT2DCA;
   
   TH1D * h_SCTmultiplicity;
   TH1D * h_TRTmultiplicity;
   TH1D * h_Pixelmultiplicity;
   TH1I * h_asymTRTAC;
   TH1I * h_asymTRTCA;
   TH1I * h_asymPixelAC;
   TH1I * h_asymPixelCA;
   
   TH1I * h_BCMPosAC;
   TH1I * h_BCMPosCA;
   TH1I * h_BCMWidthAC;
   TH1I * h_BCMWidthCA;
   
   ClassDef(plottingID,0);
};

#endif

#ifdef plottingID_cxx
void plottingID::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

  // Associate the TTreeReader with the tree we want to read
   fReader.SetTree(tree);

   // Set object pointer
   VertexType = 0;
   VertexChi2 = 0;
   VertexDoF = 0;
   VertexX = 0;
   VertexY = 0;
   VertexZ = 0;
   VertexTracks = 0;
   VertexpT2 = 0;
   HitsOnDisks = 0;
   HitsPixel = 0;
   BCMchan = 0;
   BCMBCID = 0;
   BCMpulse1Pos = 0;
   BCMpulse1Width = 0;
   BCMpulse2Pos = 0;
   BCMpulse2Width = 0;
   
   int number = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("TimeStamp", &timeStamp, &b_timeStamp);
   fChain->SetBranchAddress("LBNumber", &LBNumber, &b_LBNumber);
   fChain->SetBranchAddress("BCID", &bcid, &b_bcid);
   fChain->SetBranchAddress("ACTrigger", &passesACTrigger, &b_passesACTrigger);
   fChain->SetBranchAddress("CATrigger", &passesCATrigger, &b_passesCATrigger);
   fChain->SetBranchAddress("SCTHitsOnDisks", &HitsOnDisks, &b_HitsOnDisks);
   fChain->SetBranchAddress("BCMprescale", &BCMprescale, &b_BCMprescale);
   fChain->SetBranchAddress("GoodLB", &GRL, &b_GRL);
   fChain->SetBranchAddress("isoTrigger", &isotrigger, &b_isotrigger);
   fChain->SetBranchAddress("VertexTracks", &VertexTracks, &b_VertexTracks);
   fChain->SetBranchAddress("VertexType", &VertexType, &b_VertexType);
   fChain->SetBranchAddress("VertexChi2", &VertexChi2, &b_VertexChi2);
   fChain->SetBranchAddress("VertexDoF", &VertexDoF, &b_VertexDoF);
   fChain->SetBranchAddress("VertexX", &VertexX, &b_VertexX);
   fChain->SetBranchAddress("VertexY", &VertexY, &b_VertexY);
   fChain->SetBranchAddress("VertexZ", &VertexZ, &b_VertexZ);
   fChain->SetBranchAddress("VertexpT2", &VertexpT2, &b_VertexpT2);
   fChain->SetBranchAddress("TRTHitsECA", &HitsTRTECA, &b_HitsTRTECA);
   fChain->SetBranchAddress("TRTHitsECC", &HitsTRTECC, &b_HitsTRTECC);
   fChain->SetBranchAddress("PixelHitsOnDisks", &HitsPixel, &b_HitsPixel); 
   fChain->SetBranchAddress("BCMchan", &BCMchan, &b_BCMchan); 
   fChain->SetBranchAddress("BCMBCID", &BCMBCID, &b_BCMBCID); 
   fChain->SetBranchAddress("BCMpulse1Pos", &BCMpulse1Pos, &b_BCMpulse1Pos); 
   fChain->SetBranchAddress("BCMpulse1Width", &BCMpulse1Width, &b_BCMpulse1Width); 
   fChain->SetBranchAddress("BCMpulse2Pos", &BCMpulse2Pos, &b_BCMpulse2Pos); 
   fChain->SetBranchAddress("BCMpulse2Width", &BCMpulse2Width, &b_BCMpulse2Width); 
}

Bool_t plottingID::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

#endif // #ifdef plottingID_cxx
